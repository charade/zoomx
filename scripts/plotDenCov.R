#!/usr/bin/env Rscript

####Externalities####
ptm=proc.time() #time keeper

set_colClass <- function(d, colClasses) {
  colClasses <- rep(colClasses, len=length(d))
  d[] <- lapply(seq_along(d), function(i) switch(colClasses[i],
                                                 numeric=as.numeric(d[[i]]),
                                                 character=as.character(d[[i]]),
                                                 Date=as.Date(d[[i]], origin='1970-01-01'),
                                                 POSIXct=as.POSIXct(d[[i]], origin='1970-01-01'),
                                                 factor=as.factor(d[[i]]),
                                                 as(d[[i]], colClasses[i]) ))
  return(d)
}

myrequire = function(pkg, repo="CRAN", lib=Sys.getenv("R_LIBS_USER"), ...){
  cat("requiring package", pkg, "\n")
  tryCatch(suppressMessages(library(pkg,character.only=T)), error=function(e) {
    print(e)
    if(repo!="CRAN"){
      source("http://bioconductor.org/biocLite.R")
      biocLite(pkg,...)
    } else {
      install.packages(pkg,repo="http://cran.us.r-project.org",lib=lib,...)
    }
  })
  tryCatch(suppressMessages(library(pkg,character.only=T)), error=function(e) {
    print(e)
    stop(pkg," was not installed and cannot install on the fly!\n")
  })
}

#for(p in c("optparse","ggplot2","rjson","fitdistrplus","car","RColorBrewer")) myrequire(p)
for(p in c("optparse","ggplot2","rjson","fitdistrplus","car","RColorBrewer")) require(p,character.only=T)

option_list2 <- list(
  make_option(c("-p", "--outPrefix"), default="none",
              help="outPrefix for separate plots separated by ';', none=mcvFiles [default %default]"),
  make_option(c("-v", "--overlapFactor"), type="integer", default=1,
              help="a factor of 2 means windows are half overlapping [default %default]"),
  make_option(c("-d", "--referenceGridsize"), type="integer", default=10000,
              help="gridSize used ad reference [default %default]"),
  make_option(c("-n", "--genomeSize"), type="numeric", default=3200000000,
              help="genomeSize for plot [default %default]"),
  make_option(c("-f", "--format"), default="tiff",
              help="output format, [default %default]"),
  make_option(c("-s", "--plotScale"), default=1,
              help="output scaling, [default %default]"),
  make_option(c("-l", "--label"), default="none",
              help="output label, [default %default]"),
  make_option(c("-q", "--noQuiet"), action="store_true", default=FALSE,
              help="show verbose, [default %default]"),
  make_option(c("-a", "--debug"), action="store_true", default=FALSE,
              help="save debug, [default %default]")
)

parser <- OptionParser(usage = "%prog [options] mcvFile1;mcvFile2;... gridSize1;gridSize2;... cfgFile",
                       option_list=option_list2)
cat("-Info: invoking command:",commandArgs(),"\n")
args <- commandArgs(trailingOnly = TRUE)
cmd = parse_args(parser, args, print_help_and_exit = TRUE, positional_arguments = TRUE)
if(length(cmd$args)!=3 & !cmd$options$debug){ print_help(parser); quit() }

mcvFiles=strsplit(cmd$args[1],";")[[1]]
gridSize=as.integer(strsplit(cmd$args[2],";")[[1]])
cfgFile=cmd$args[3]
outPrefix=strsplit(cmd$options$outPrefix,";")[[1]]
format=cmd$options$format
genomeSize=cmd$options$genomeSize
overlapFactor=cmd$options$overlapFactor
referenceGridsize=cmd$options$referenceGridsize
plotScale=cmd$options$plotScale
label=cmd$options$label
debug=cmd$options$debug
noQuiet=cmd$options$noQuiet
if(outPrefix=="none") outPrefix=mcvFiles
if(label=="none") label=""

if(debug){
  setwd("~/ws/zoomx/03_Stats")
  mcvFiles = c("NA12878_WGS_phased_possorted_bam.bam.w100Ks50K.mcv","NA12878_WGS_phased_possorted_bam.bam.w100Ms50M.mcv",
               "NA12878_WGS_phased_possorted_bam.bam.w10Ks5K.mcv","NA12878_WGS_phased_possorted_bam.bam.w10Ms5M.mcv",
               "NA12878_WGS_phased_possorted_bam.bam.w1Ms500K.mcv")
  outPrefix = mcvFiles
  cfgFile = "NA12878_WGS_phased_possorted_bam.bam.gridScan.cfg.json"
  gridSize = as.integer(c("100000","100000000","10000","10000000","1000000"))
  referenceGridsize = 10000
  overlapFactor = 1
  genomeSize = 3200000000
  plotScale = 10
  format = "tiff"
  label = "NA12878_"
}
colScheme="Dark2"
cols <- brewer.pal(8, colScheme)[3:4]
colnames = paste(label,c("Fit","Empirical"),sep="")
names(cols) = colnames

mainTextSize=14
legendTextSize=16
mainLineSize=1.5
plotMargin = unit(c(0.28,0.28,0.28,0.28), "in")
cfgOptions = fromJSON(readLines(cfgFile))
averageFragLength = cfgOptions$mLf
fragNumber = cfgOptions$aNf
coveragePerBlock = round(fragNumber / (genomeSize/gridSize) )
names(coveragePerBlock) = as.character(gridSize)
tickFactor = overlapFactor / (gridSize/referenceGridsize)  # times tick to correct for x labels

data = lapply(mcvFiles, "read.table")
cov = lapply(seq_along(data), FUN=function(x){ data[[x]][,4][data[[x]][,4]>10] } )

plotDenCov = function(i, cov, outPrefix, coveragePerBlock, cols, tickFactor, plotScale, format, showDensity=FALSE){
  prefix=outPrefix[[i]];
  colnames=names(cols)
  pois.fit = fitdist(cov[[i]],"pois"); cpb = pois.fit$estimate;
  Coverage = cov[[i]]; tf=tickFactor[[i]]; gs=names(coveragePerBlock)[[i]] #genome size
  xlim=round(c(0.25*cpb,1.75*cpb)/10)*10  #round to 10-based
  xticks=seq(xlim[1],xlim[2],by=50/tf) 
  xticklabs=paste(as.integer(seq(xlim[1]*tf,xlim[2]*tf,by=50)))
  ylim=c(0, 1.25*max(dpois(seq(1,1.5*cpb),lambda=cpb)))
  cat("fitted Poisson lamba=", cpb, "\n")
  cat("xlim:", xlim, "\n")
  cat("ylim:", ylim, "\n")
  
  message("plotFile1=",paste(prefix, "density", format, sep="."))
  p0 = qplot(Coverage, geom = 'blank') +   
    geom_histogram(aes(y = ..density..), alpha = 0.4, fill="blue", color="gray", binwidth=4/tf)
  if(showDensity) {
    p0 = p0 + geom_line(aes(y = ..density.., colour=colnames[2]), stat="density", size=mainLineSize) } #Empricial
  p0 = p0 + geom_line(aes(x = seq(xlim[1], xlim[2]), 
                          y = dpois(seq(xlim[1], xlim[2]), lambda=cpb), 
                          colour=colnames[1]),
                          size=mainLineSize) +
    theme_classic() +
    scale_colour_manual(name = '', values = cols ) +
    scale_x_continuous('Count', breaks=xticks, label=xticklabs, limit=xlim) +
    ylim(ylim) + ggtitle(paste("Molecule Count per", as.integer(gs)/1000,"KB")) + ylab("Density") + 
    theme(legend.position = c(0.85,0.85), text=element_text(size=legendTextSize)) + 
    theme(plot.margin=plotMargin) + 
    theme(plot.title = element_text(hjust=0.5, size=as.integer(mainTextSize), face="bold")) #+
  #ggsave(filename=paste(prefix, format, sep="."),plot=p0,w=5.6, h=5.6,units="in",dpi=72*plotScale,
  #       type="Xlib", compression="lzw")
  tiff(filename=paste(prefix, "density", format,  sep="."),w=5.6*72*plotScale,h=5.6*72*plotScale,units="px",res=72*plotScale, type="cairo", compression="lzw")
  print(p0)
  dev.off()
  
  message("plotFile2=",paste(prefix, "qqpois", format, sep="."))
  tiff(filename=paste(prefix, "qqpois", format,  sep="."),w=5.6*72*plotScale,h=5.6*72*plotScale,units="px",res=72*plotScale,
       type="cairo", compression="lzw")

  cv = sample(cov[[i]],min(10000,length(cov[[i]])))
  qqplot(y=cv, x=qpois(ppoints(length(cv)), lambda=pois.fit$estimate),
                       xlim=xlim, ylim=xlim, 
                       xlab="Poisson Fit", ylab="Emprical Coverage")
  qqline(y=cv, distribution=function(p) qpois(p, lambda=pois.fit$estimate))
  #library("BoutrosLab.plotting.general") #not working for some reason
  #tmp.confidence.interval <- create.qqplot.fit.confidence.interval(cv);
  #lines(tmp.confidence.interval$z, tmp.confidence.interval$upper.pw, lty = 2, col = "brown")
  #lines(tmp.confidence.interval$z, tmp.confidence.interval$lower.pw, lty = 2, col = "brown")
  #lines(tmp.confidence.interval$z[tmp.confidence.interval$u], 
  #      tmp.confidence.interval$upper.sim, lty = 2, col = "blue")
  #lines(tmp.confidence.interval$z[tmp.confidence.interval$l], 
  #      tmp.confidence.interval$lower.sim, lty = 2, col = "blue")
  #legend(1, -1.5, c("simultaneous", "pointwise"), col = c("blue", "brown"), lty = 2, bty = "n")
  #qqPlot(cv, distribution="pois", lambda=pois.fit$estimate, main="", ylab="Empirical Coverage", xlab="Poisson Fit")
  dev.off()
} #plotDenCov(3,cov,outPrefix,coveragePerBlock,cols=cols, tickFactor=tickFactor, plotScale=plotScale, format=format)

sapply(seq(length(mcvFiles)), "plotDenCov", cov, outPrefix, coveragePerBlock, cols=cols, tickFactor=tickFactor, plotScale=plotScale, format=format)



quit()

#obsolete proof-of-idea codes



i=3

#k=ks.test(cov[[i]]+rnorm(length(cov[[i]]))/1000, "ppois", mean(cov[[i]]))



descdist(cov[[i]], discrete = FALSE)

m1 <- glm(Freq ~ cv, family="poisson", data=tb)
summary(m1)

#In the classic textbook by Johnson and Wichern (Applied Multivariate Statistical Analysis, Third Edition, 1992, p. 164), it says:

#All measures of goodness-of-fit suffer the same serious drawback. When the sample size is small, only the most aberrant behaviors will be identified as lack of fit. On the other hand, very large samples invariably produce statistically significant lack of fit. Yet the departure from the specified distributions may be very small and technically unimportant to the inferential conclusions.


testDenCov = function(i, cov, outPrefix, coveragePerBlock) {
  cv = cov[[i]]; prefix=outPrefix[[i]]; cpb=coveragePerBlock[[i]]
  #cv_select = seq(round(0.5*median(cv)),round(1.5*median(cv))) #exclude outliers
  #tcv = cv[ cv %in% cv_select ]
  cv_tb = as.data.frame(table(cv))
  cat("fitdistr fitting", i, "-th figure", paste(prefix, "pdf", sep="."), "\n")
  my.mle<-fitdistr(cv, densfun="poisson")
  print(my.mle)
  print(my.mle$loglik)
  cv_mean = my.mle$estimate
  
  tb$cv = as.numeric(as.character(tb$cv))
  tb$Freq = as.numeric(as.character(tb$Freq))
  probs = dpois(tb$cv,lambda=cv_mean)
  chisq.test(x=tb$Freq, p=probs) #cannot have expected frequency=0
  #chisq.test(x=tb$cv, p=probs) #cannot have expected frequency=0
  min_freq=200
  x=c(tb$Freq[tb$Freq>min_freq],sum(tb$Freq[tb$Freq<=min_freq])
  p=c(probs[tb$Freq>min_freq],sum(probs[tb$Freq<=min_freq])
  chisq.test(x=x, p=p)
  chisq.test(x=x, p=p, simulate.p.value = T)
  for(j in seq(cv_min, cv_max)) if(! j %in% tb$cv) tb=rbind(tb, data.frame(cv=j,Freq=0))
  tb$Expect = dpois(tb$tcv, median(tcv)) # or mean of (tcv)
  tb$P = tb$Expect/(sum(tb$Expect))
  cat("fitdistrplus fitting", i, "-th figure", paste(prefix, "pdf", sep="."), "\n")
  pois.fit = fitdist(cv,"pois")
  #pois.fit = fitdist(cv,"pois",method="mme")
  plot(pois.fit)
  gofstat(pois.fit)
  nbinom.fit = fitdist(cv,"nbinom")
  plot(nbinom.fit)
  gofstat(nbinom.fit)
  
  Test with simulated Poisson
  n.sims = 10

  stats <- replicate(n.sims, {
    r <- rpois(n = length(cov[[i]])
                  , lambda = pois.fit$estimate["lambda"]
    )
    as.numeric(ks.test(r+rnorm(length(r),0,0.001)
                       , "ppois"
                       , lambda = pois.fit$estimate["lambda"])
    )
  })

  fit <- logspline(stats[1,])

  1 - plogspline(ks.test(cv+rnorm(length(r),0,0.001)
                         , "ppois"
                         , lambda = pois.fit$estimate["lambda"])$statistic
                 , fit
  )
  

  cat("chi-square testing", i, "-th figure", paste(prefix, "pdf", sep="."), "\n")
  c=chisq.test(tb$Freq, p=tb$P, simulate.p.value=TRUE)
  print(c)
  cat("K-S testing", i, "-th figure", paste(prefix, "pdf", sep="."), "\n")
  k=ks.test(cv+rnorm(length(cv))/1000, "ppois", mean(cv))
  print(t)
  cat("theory: empirical mean=", cpb, "\n")
  cat("xlim:", xlim, "\n")
  cat("ylim:", ylim, "\n")
}

library(MASS)
sapply(seq(length(mcvFiles)), "testDenCov", cov, outPrefix, coveragePerBlock)


# plotDenCov = function(i, cov, files, coveragePerBlock){
#   cols <- brewer.pal(3, "Set1")
#   cv = cov[[i]]; prefix=files[[i]]; cpb=coveragePerBlock[[i]]
#   cat("plotting",paste(prefix, "png", sep="."),"\n")
#   png(paste(prefix, "png", sep="."), h=1000, w=1000, pointsize=20)
#   hp = hist(cv, breaks=100, ylim=c(0,0.01), xlim=c(0,2*cpb), prob=T, col=cols[1])            # prob=TRUE for probabilities not counts
#   print(hp)
#   poix = seq(min(hp$breaks),max(hp$breaks)) #hp$breaks
#   poiy = dpois(poix,lambda=cpb) #cpbp coveragePerBlock
#   #lines(density(cv), col=cols[2])             # add a density estimate with defaults
#   lines(density(cv, adjust=2), lty="dotted", ylim=hp$ylim*2, col=cols[2])   # add another "smoother" density
#   #points(poix, poiy, col=cols[3])
#   lines(poix, poiy, lty="dotted", col=cols[3])   # add another "smoother" density
#   # Add a legend using the nice sample labeles rather than the full filenames.
#   #legend("topright", legend=labels, col=cols, lty=1, lwd=4)
#   dev.off()
# }
# 
# lapply(seq_along(cov), "plotDenCov", cov, files, coveragePerBlock)

#ggplot2
# create some data to work with

x = rnorm(1000);

# overlay histogram, empirical density and normal density
p0 = qplot(x, geom = 'blank') +   
  geom_line(aes(y = ..density.., colour = 'Empirical'), stat = 'density') +  
  stat_function(fun = dnorm, aes(colour = 'Normal')) +                       
  geom_histogram(aes(y = ..density..), alpha = 0.4) +                        
  scale_colour_manual(name = 'Density', values = c('red', 'blue')) + 
  theme(legend.position = c(0.85, 0.85))

print(p0)
