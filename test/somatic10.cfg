#!/bin/bash
#if zoomx python entry-points (parse10x, gridScan, refineBreak)
#and utility scripts are not in your PATH
#NOTES:
#  ZOOMXPKG has to be set per user here
#  caseCovFile/controlCovFile is ignored if specified as "none" or it is not found
ZOOMXPKG=$HOME/setup/zoomx                #where the zoomx package is located
ZOOMXPATH=$ZOOMXPKG/zoomx                 #where the python libs of zoomx are located
SCRIPTPATH=$ZOOMXPKG/scripts              #where the uitlity scripts zoomx are located
PATH=$SCRIPTPATH:$PATH                    #add script path to PATH
mydate=$(LC_ALL=en_US.utf8 date +%Y%b%d)  #get today's date
mode=somatic                              #is this somatic or germline run?
essentialStages=(controlCov caseCov controlParse caseParse controlScan caseScan controlDistRef controlTransRef caseDistRef caseTransRef)                                   #essential stages for a somatic run
extendedStages=(distDiffMix transDiffMix caseDistPlot caseTransPlot) #extended stages for a somatic run
ref=human_g1k_v37.fasta                   #prefix to the grid file, the acutal ucsc reference fasta is not required
grid=$ref.bed.w10Ks5K                     #scan grid file (*required input*), e.g. bed file of 10 Kbp half-overlap sliding windows
covGrid=$ref.bed.w100s100                 #coverage grid file (*required input*), e.g. a bed file of 100 bp non-overlap sliding windows
gapFile=$ref.acen+gap.bed                 #calls overlap (padded) gap regions or other regions to be excluded
outputPf=.                                #output dir
logDir=$outputPf/logs                     #output of logs
cfgDir=$outputPf/cfgs                     #output of cfgs
datDir=$outputPf/data                     #zoomx space results
resDir=$outputPf/results                  #user space results
plotDir=$outputPf/plots                   #user space plots
distSize=200000                           #min base pair between breakpoints to qualify as distal (dis) junctions
cisSize=50000                             #min base pair between breakpoints to qualify as mid-range (cis) junctions
controlPf=.                               #input dir of control bam file
controlBam=Norm7176.wg12.chr8-98M-100M.bam #control bam and bai file (*required input*), $controlBam.bai has to be in place
controlGenomeSize=2000000                 #needed for correction of Cem estimates`
controlExp=10                             #minimum expected allele frequency in control
controlMax=1000000                        #maximum expected number of junctions in control
controlCovFile=$outputPf/$controlBam.cov100 #coverage file as generated by ZoomX in controlCovScan
casePf=.                                  #input dir of case bam file
caseBam=MetB7175.wg12.chr8-98M-100M.bam   #case bam and bai file (*required input*), $caseBam.bai has to be in place
caseGenomeSize=2000000                    #needed for correction of Cem estimates`
caseExp=10                                #minimum expected allele frequency in case
caseMax=1000000                           #maximum expected number of junctions in case
caseCovFile=$outputPf/$caseBam.cov100     #coverage file as generated by ZoomX in caseCovScan
isUCSC=""                                 #turn on as --isUCSC if in UCSC format, i.e. seqname is chrX not X
maxPlot=100                               #maximum number of plots
tempDir=$outputPf/temp                    #temporary directory (will be created if non-exist, can be removed after successful run)

#following varialbes/filenames supposedly defined as in this configure file

#Required Input Files
#$controlPf/$controlBam                   #full path to the control bam file
#$controlPf/$controlBam.bai               #full path to the control bam index file (implied)
#$casePf/$caseBam                         #full path to the case bam file
#$casePf/$caseBam.bai                     #full path to the case bam index file
#$grid                                    #full path to the scan grid file
#$covGrid                                 #full path to the coverage grid file
#NOTE: $grid for 10 Kbp resolution and $covGrid for 100 bp resolution for hg37 in both UCSC and 1KG format are provided within this test folder; you can copy and use them as defaults.

#Essential Outputs
#$outputPf/$caseBam.tra.E$caseExp.refined #identified case sites represeting inter/trans chromosome (trans) events
#$outputPf/$caseBam.dist.E$caseExp.refined #identified case sites represeting intra-chromosome distal (dis) events
#$outputPf/$caseBam.cis.E$caseExp.refined #identified case sites represeting intra-chromosome mid-range (cis) events
#$outputPf/$controlBam.tra.E$caseExp.refined #identified control sites represeting inter/trans chromosome (trans) events
#$outputPf/$controlBam.dist.E$caseExp.refined #identified control sites represeting intra-chr distal (dis) events
#$outputPf/$controlBam.cis.E$caseExp.refined #identified control sites represeting intra-chr mid-range (cis) events
#$outputPf/$caseBam.tra.E$caseExp_$controlBam.tra.E$controlExp.somatic #identified somatic tra events differetiating between two refined files with the same prefix
#$outputPf/$caseBam.dist.E$caseExp_$controlBam.dist.E$controlExp.somatic #identified somatic dist events differetiating between two refined files with the same prefix
#$outputPf/$caseBam.cis.E$caseExp_$controlBam.cis.E$controlExp.somatic #identified somatic cis events differetiating between two refined files with the same prefix
#NOTE: if $controlExp is 0, $outputPf/$controlBam.jct.bedpe.tra|dis|cis were used for differentiation
#$X.somatic.Plot-$i/ #supporting molecule plots and breakpoint information for $i-th event as in junction file "$X.somatic" 

#Peripheral Outputs
#$outputPf/$controlBam.cov100            #coverage file, $covGrid plus coverage col, generated by ZoomX in controlCovScan
#$outputPf/$controlBam.multiFragBar.bed.a0   #bed file of inferred single molecule file, see FAQ of wiki for details
#$outputPf/$controlBam.multiFragBar.bed.a0.clean   #single molecule file after cleaning by coverage
#$outputPf/$controlBam.gridScan.cfg.json #json file of informative statistics from linked-read sequencing data
#$outputPf/$controlBam.IM|RR|FF.bedpe    #BEDPE file recording abnormal +-/-+, ++, and -- short read pairs
#$outputPf/$controlBam.jct.bedpe.tra|dis|cis #BEDPE file recording candidate juctions for trans, distal and cis events
#$outputPf/$caseBam.cov100                #coverage file, $covGrid plus coverage col, generated by ZoomX in caseCovScan
#$outputPf/$caseBam.multiFragBar.bed.a0   #bed file of inferred single molecule file, see FAQ of wiki for details
#$outputPf/$caseBam.multiFragBar.bed.a0.clean   #single molecule file after cleaning by coverage
#$outputPf/$caseBam.gridScan.cfg.json     #json file of informative statistics from linked-read sequencing data
#$outputPf/$caseBam.IM|RR|FF.bedpe        #BEDPE file recording abnormal +-/-+, ++, and -- short read pairs
#$outputPf/$caseBam.jct.bedpe.tra|dis|cis #BEDPE file recording candidate juctions for trans, distal and cis events

#Other Outputs
#they are midway and should be deleted once results 

#Log files
#*log.*.$mydate                           #log files of standard outs from various stage, std err is not logged
#*cfg.*.json.$mydate                      #log files in json format of actual ZoomX command lines ran
